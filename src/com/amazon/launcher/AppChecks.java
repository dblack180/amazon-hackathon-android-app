package com.amazon.launcher;

import java.io.File;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

/**
 * AppChecks.java
 * @author David Waddington
 *
 * Contains all the checks the application needs to perform.
 *
 */
public class AppChecks {
	
	public JSONObject page_val;
	public JSONHandler j_handler;
	
	/**
	 * Empty constructor because fuck populating them
	 */
	public AppChecks() {
		
	}
	
	/**
	 * AppChecks.version
	 * @author David Waddington
	 * 
	 * Checks if the version file is present locally on device
	 * @return
	 */
	public boolean version() {
		// Accesses internal storage and checks if the version file defined in Config.java is present
		File file = ThinClient.getCustomAppContext().getFileStreamPath(Config.VERSION_FILE);
		// Return true if it exists else return false
		return file.exists();
	}
	
	/**
	 * AppChecks.newerVersion
	 * @author David Waddington
	 * 
	 * Checks to see if the version published online is newer than the version stored locally
	 * @return
	 */
	public boolean newerVersion() {
		JSONObject local_val = null;
		String version = null;
		j_handler = new JSONHandler();
		// Runs getJSONFromUrl in seperate thread so we don't have network on UI thread
		Thread t = new Thread(new Runnable() {
			public void run() {
				setPageVal(j_handler.getJSONFromUrl(Config.API_URL + Config.API_VERSION + Config.APP_IDENTIFIER));
			}
		});
		t.start();
		/*
		 * Blocks function until thread returns and is joined back to the UI thread
		 */
		try {
			t.join();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
		local_val = j_handler.getJSONFromFileInternalStorage(Config.VERSION_FILE);
		
		/*
		 * Gets the version from the K->V map that is JSON
		 * Converts the version to a floating point number (because it contains decimals)
		 * Compares the local version float to the online version float
		 * Returns true if the online version is higher
		 */
		try {
			version = page_val.getString("version");
			if(Float.parseFloat(version) > Float.parseFloat(local_val.getString("version"))) {
				return true;
			} else {
				return false;
			}
		} catch (JSONException e) {
			Log.e("AppCheck 'newerVersion()'", "JSON Exception " + e.toString());
			return false;
		}
	}
	
	/**
	 * AppCheck.setPageVal
	 * @author David Waddington
	 * @param val
	 * 
	 * Allows the thread in the newerVersion method to alter the class' variable
	 */
	public void setPageVal(JSONObject val) {
		page_val = val;
	}
}
