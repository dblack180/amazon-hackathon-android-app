package com.amazon.launcher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.cordova.api.CallbackContext;
import org.apache.cordova.api.CordovaPlugin;
import org.apache.cordova.api.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;

/**
 * CustFileInput.java
 * @author David Waddington
 * 
 * Custom cordova plugin that allows cordova apps to access to app's local storage
 *
 */
public class CustFileInput extends CordovaPlugin {
	@Override
	/**
	 * CustFileInput.execute
	 * @author David Waddington
	 * 
	 * Takes the action, arguments and a callback.
	 * Runs the method in the class that corresponds to the action passed in.
	 * Returns the output of the method to the callback with an OK status.
	 * 
	 */
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
		PluginResult.Status status = PluginResult.Status.OK;
		if(action.equals("open")) {
			String fileURI = args.getString(0);
			String retval = this.open(fileURI);
			callbackContext.sendPluginResult(new PluginResult(status, retval));
			return true;
		} else if(action.equals("path")) {
			String retval = this.path();
			callbackContext.sendPluginResult(new PluginResult(status, retval));
			return true;
		}
		return false;
	}
	
	/**
	 * CustFileInput.open
	 * @author David Waddington
	 * @param fileURI
	 * 
	 * Allows cordova JS to open a file that's stored in the app's internal storage.
	 * Returns the contents of the file in a string.
	 * 
	 * @return
	 */
	private String open(String fileURI) {
		Context inFS = cordova.getActivity().getApplicationContext();
		String output = "";
		StringBuffer buffer = new StringBuffer();
		try {
			BufferedReader inputReader = new BufferedReader(new InputStreamReader(inFS.openFileInput(fileURI)));
			while ((output = inputReader.readLine()) != null) {
				buffer.append(output + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return buffer.toString();
	}
	
	/**
	 * CustFileInput.path
	 * 
	 * Returns a string representation of the app's internal storage directory
	 * This is mainly used to load the downloaded web files for the app
	 * 
	 * @return
	 */
	private String path() {
		String path = Config.APP_DIRECTORY;
		return path;
	}
}
