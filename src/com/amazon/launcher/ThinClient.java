/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package com.amazon.launcher;

import android.content.Context;
import android.os.Bundle;

import org.apache.cordova.*;

import com.amazon.launcher.async.*;

public class ThinClient extends DroidGap
{
	private static Context context;
	public Functions function;
	
    @Override
    public void onCreate(Bundle savedInstanceState)
    {	
    	super.onCreate(savedInstanceState);
    	super.loadUrl("file:///android_asset/loader/loader.html");
    	
    	context = this.getApplicationContext();
    	AppChecks check = new AppChecks();
    	
    	if(check.version()) {
    		UpdateVersion u_ver = new UpdateVersion();
    		u_ver.execute();
    		
    	} else {
    		DownloadVersion d_ver = new DownloadVersion();
    		DownloadFullApp f_app = new DownloadFullApp();
    		d_ver.execute();
    		f_app.execute();
    	}
    	
    }
    
    public static Context getCustomAppContext() {
    	return context;
    }
}

