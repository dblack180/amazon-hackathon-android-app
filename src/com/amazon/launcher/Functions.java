package com.amazon.launcher;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

/**
 * Functions.java
 * @author David Waddington
 *
 * Contains recently used functions for the app
 *
 */
public class Functions {

	JSONHandler j_handler;
	JSONObject j_object;
	
	/**
	 * Empty constructor because fuck populating.
	 */
	public Functions() {
		
	}
	/**
	 * Functions.getVersion
	 * @author David Waddington
	 * 
	 * Used in DownloadVersion.java in the async package.
	 * 
	 * Downloads the version JSON from the API and stores to the app's internal storage
	 */
	public void getVersion() {
		Log.i("'getVersion()'", "Starting to download version file");
		new Thread(new Runnable() {
			public void run() {
				JSONHandler json_handler = new JSONHandler();
				JSONObject page_json = new JSONObject();
				page_json = json_handler.getJSONFromUrl(Config.API_URL + Config.API_VERSION + Config.APP_IDENTIFIER);
				json_handler.writeJSONFileInternalStorage(page_json, Config.VERSION_FILE);
				Log.i("Version Check", "Local Version file written successfully");
			}
		}).start();
	}
	
	/**
	 * Functions.unpackZip
	 * @author David Waddington
	 * @param path
	 * @param zipname
	 * @param destination
	 * 
	 * Takes in a path to the zip, the zip file's name and where it should be extracted to
	 * Extracts the zip in the passed in destination.
	 * 
	 * @return
	 */
	public boolean unpackZip(String path, String zipname, String destination) {
		InputStream is;
		ZipInputStream zis;
		try {
			String filename;
			/*
			 * We don't have a direct URI to assets so we can extract there if we need to with this
			 */
			if(path.equals("assets")) {
				// This uses some android package management features
				zis = new ZipInputStream(ThinClient.getCustomAppContext().getAssets().open(zipname));
			} else {
				/*
				 * Open a FileInputStream to the zip file that's being targetted.
				 * Pass it into the ZipInputStream so we know we're dealing with a .zip file
				 */
				is = new FileInputStream(path + zipname);
				zis = new ZipInputStream(new BufferedInputStream(is));
			}
			ZipEntry ze;
			byte[] buffer = new byte[1024];
			int count;
			
			/*
			 * Get details of the zip file here.
			 */
			while((ze = zis.getNextEntry()) != null) {
				// We pull out it's filename.
				filename = ze.getName();
				
				/*
				 * We check to see if the part of the zip we're in is a directory.
				 * If it is then we create a new directory on the device for it's contents.
				 */
				if(ze.isDirectory()) {
					File fmd = new File(destination + filename);
					fmd.mkdirs();
					continue;
				}
				
				// Creates an output stream for us to start pushing files to
				FileOutputStream fout = new FileOutputStream(destination + filename);
				
				/*
				 * Loop through the entire zip file.
				 * Fill up the buffer
				 * Write the buffer to the app's internal storage
				 */
				while((count = zis.read(buffer)) != -1) {
					/**
					 * -- USEFUL KNOWLEDGE -- 
					 * Sometimes the buffer won't be 100% full.
					 * So we set the third variable of this .write() to count
					 * This means that we only write bits 0 to the end bit of the stream in the buffer
					 * Which means we don't take up space that we didn't actually need.
					 */
					fout.write(buffer, 0, count);
				}
				// Close and tidy things up
				fout.close();
				zis.closeEntry();
			}
			// Final tidying
			zis.close();
		} catch(IOException e) {
			Log.e("Unzipping", "IOException: " + e.toString());
			return false;
		}
		Log.i("Unzipping", "Completed unzipping");
		return true;
	}
	
	/**
	 * Functions.getURLEndpoint()
	 * @author David Waddington
	 * 
	 * Gets the app's host from the main API store.
	 * This facilitates the download of files from the app's specific EC2 instance.
	 * This will also allow further connections to be facilitated.
	 * 
	 * @return host_ip for app
	 */
	public String getURLEndpoint() {
		String hostip = null;
		j_handler = new JSONHandler();
		
		/*
		 * We don't want to run network on the UI thread. Toss it into it's own thread
		 */
		Thread t = new Thread(new Runnable() {
			public void run() {
				setJObject(j_handler.getJSONFromUrl(Config.API_URL + Config.API_HOSTIP + Config.APP_IDENTIFIER));
			}
		});
		t.start();
		/*
		 * Block further execution of the method until the thread has terminated and joined the UI thread
		 */
		try {
			t.join();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
		/*
		 * Attempts to get the host_ip out of the JSON that is returned.
		 */
		try {
			hostip = j_object.getString("host_ip");
			return hostip;
		} catch (JSONException e) {
			Log.e("Functions 'getURLEndpoint()'", "JSON Exception " + e.toString());
			return null;
		}
	}
	
	/**
	 * Helper function to allow the thread in getURLEndpoint to write to class variable
	 * @param obj
	 */
	private void setJObject(JSONObject obj) {
		j_object = obj;
	}
}
