package com.amazon.launcher.async;

import com.amazon.launcher.Functions;

import android.os.AsyncTask;

/**
 * DownloadVersion.java
 * @author David Waddington
 *
 * Downloads app's JSON version file and stores it to the app's internal storage.
 * AsyncTask so we don't have networking on the UI thread.
 * 
 */
public class DownloadVersion extends AsyncTask<Void, Void, Void>{

	@Override
	protected Void doInBackground(Void... params) {
		Functions function = new Functions();
		function.getVersion();
		return null;
	}
}
