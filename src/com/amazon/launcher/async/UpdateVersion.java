package com.amazon.launcher.async;

import com.amazon.launcher.AppChecks;
import com.amazon.launcher.Functions;

import android.os.AsyncTask;
import android.util.Log;

/**
 * UpdateVersion.java
 * @author David Waddington
 * 
 * Checks if there's a newer version of the app published online.
 * If there is then it downloads the new app version.
 * [TODO] This needs the intelligent file downloading code written and added.
 *
 */
public class UpdateVersion extends AsyncTask<Void, Void, Void>{

	@Override
	protected Void doInBackground(Void... params) {
		AppChecks check = new AppChecks();
		Functions function = new Functions();
		if(check.newerVersion()) {
			Log.i("Version Control", "Newer version found for app.");
			function.getVersion();
		} else {
			Log.i("Version Control", "App is up to date");
		}
		return null;
	}
}
