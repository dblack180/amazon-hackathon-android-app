package com.amazon.launcher.async;

import com.amazon.launcher.Config;
import com.amazon.launcher.DownloadFileHandler;
import com.amazon.launcher.Functions;

import android.os.AsyncTask;

/**
 * DownloadFullApp.java
 * @author David Waddington
 * 
 * Downloads app's full zip file from it's EC2 host IP (defined by the API).
 * Run as an AsyncTask so we don't fuck the UI thread over.
 * Yay for no networking on a UI thread.
 *
 */
public class DownloadFullApp extends AsyncTask<Void, Void, Void>{

	@Override
	protected Void doInBackground(Void... params) {
		DownloadFileHandler dl_handler = new DownloadFileHandler();
		Functions function = new Functions();
		dl_handler.downloadZipToInternalStorage("http://" + function.getURLEndpoint() + "/", "full.zip");
		function.unpackZip(Config.APP_DIRECTORY, "full.zip", Config.APP_DIRECTORY);
		return null;
	}

}
