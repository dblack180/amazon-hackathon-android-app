package com.amazon.launcher;
/**
 * Config File
 * @author David Waddington
 * 
 * Provides variables used throughout the app that are subject to change in the platform.
 *
 */
public interface Config {
	public static final String APP_DIRECTORY = "/data/data/com.amazon.launcher/files/";
	public static final String APP_IDENTIFIER = "1";
	public static final String VERSION_FILE = "version.json";
	public static final String API_URL = "http://amazon.dietpop.co.uk/index.php/api/appAPI/";
	public static final String API_VERSION = "appVersion/";
	public static final String API_HOSTIP = "appHost/";
}
