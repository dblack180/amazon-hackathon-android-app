package com.amazon.launcher;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
 
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
 
import android.content.Context;
import android.util.Log;

/**
 * JSONHandler.java
 * @author David Waddington
 *
 * Offers helper functions to get JSON from various data sources.
 *
 */
public class JSONHandler {
 
    static InputStream is = null;
    static JSONObject jObj = null;
    static String json = "";
 
    // constructor
    public JSONHandler() {
 
    }
    
    /**
     * JSONHandler.getJSONFromUrl
     * @author David Waddington
     * @param url
     * 
     * Attempts to get JSON from URL passed into method.
     * Returns as JSONObject for easy use later.
     * 
     * @return
     */
    public JSONObject getJSONFromUrl(String url) {
 
        // Making HTTP request
        try {
            // defaultHttpClient
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
 
            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();           
 
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
 
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
 
        // try parse the string to a JSON object
        try {
            jObj = new JSONObject(json);
        } catch (JSONException e) {
            Log.e("JSON Handler", "Error parsing data " + e.toString());
        }
 
        // return JSON String
        return jObj;
    }
    
    /**
     * JSONHandler.getJSONFromFileInternalStorage
     * @author David Waddington
     * @param filename
     * 
     * Attempts to get JSON from file that's stored in the app's internal storage.
     * Returns as JSONObject for easy use later.
     * 
     * @return
     */
    public JSONObject getJSONFromFileInternalStorage(String filename) {
    	Context context = ThinClient.getCustomAppContext();
    	try {
    		BufferedReader inputReader = new BufferedReader(new InputStreamReader(context.openFileInput(filename)));
    		String line = null;
    		StringBuilder buffer = new StringBuilder();
    		while((line = inputReader.readLine()) != null) {
    			buffer.append(line + "\n");
    		}
    		inputReader.close();
    		json = buffer.toString();
    	} catch(FileNotFoundException e) {
    		Log.e("JSON Parser", "getJSONFromFileInternalStorage - File not found: " + e.toString());
    	} catch (IOException e) {
    		Log.e("JSON Handler", "getJSONFromFileInternalStorage - IOException: " + e.toString());
		}
    	
    	try {
    		jObj = new JSONObject(json);
    	} catch(JSONException e) {
    		Log.e("JSON Handler", "Failed to create JSON String from InternalStorage file");
    	}
    	
    	return jObj;
    }
    
    /**
     * JSONHandler.writeJSONFileInternalStorage
     * @author David Waddington
     * @param json
     * @param filename
     * 
     * Writes a JSONObject to a file in the app's internal storage.
     * 
     */
    public void writeJSONFileInternalStorage(JSONObject json, String filename) {
    	Context context = ThinClient.getCustomAppContext();
    	try {
    		@SuppressWarnings("static-access")
			FileOutputStream output = context.openFileOutput(filename, context.MODE_PRIVATE);
    		output.write(json.toString().getBytes());
    		output.close();
    	} catch(IOException e) {
    		Log.e("JSON Handler", "Failed to write JSON String to InternalStorage file");
    	}
    }
}
