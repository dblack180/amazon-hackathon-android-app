package com.amazon.launcher;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.content.Context;
import android.util.Log;

/**
 * DownloadFileHandler.java
 * @author David Waddington
 * 
 * Provides methods to download files to the app.
 *
 */
public class DownloadFileHandler {
	static InputStream is = null;
	
	/**
	 * Empty constructor because fuck populating
	 */
	public DownloadFileHandler() { 
		
	}
	
	/**
	 * DownloadFileHandler.downloadZipToInternalStorage
	 * @author David Waddington
	 * @param url
	 * @param filename
	 * 
	 * Downloads zip with filename passed in from the url passed in.
	 * Stores downloaded zip file to the app's internal storage.
	 */
	public void downloadZipToInternalStorage(String url, String filename) {
		Context context = ThinClient.getCustomAppContext();
		
		try {
			/*
			 * Forms a URL.
			 * Connects to the URL and opens a connection.
			 */
			URL connectionURL = new URL(url.toString() + filename.toString());
			HttpURLConnection c = (HttpURLConnection)connectionURL.openConnection();
			c.setRequestMethod("GET");
			c.setDoOutput(true);
			c.connect();
			
			/*
			 * Creates a FileOutpuStream to the app's internal storage area.
			 * Creates an input stream from the content passed in from the HTTP connection.
			 */
			FileOutputStream output = context.openFileOutput(filename, context.MODE_PRIVATE);
			InputStream is = c.getInputStream();
			
			/*
			 * Gives us a buffer to store parts of the downloaded zip file in.
			 * Loops through the buffer and writes that specific part to the output stream 
			 */
			byte[] buffer = new byte[1024];
			int len1 = 0;
			while((len1 = is.read(buffer)) != -1) {
				output.write(buffer, 0, len1);
			}
			
			// Close and tidy everything up
			output.flush();
			output.close();
			is.close();
		} catch(Exception e) {
			Log.e("DownloadFileHandler", "Exception in downloadZipToInternalStorage: " + e.toString());
		}
	}
}
