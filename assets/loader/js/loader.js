var loader = {

  counter: 0,

  initialize: function() {
    this.animateLoader(function() { window.location = "../www/index.html"; });
  },

  animateLoader: function(callback) {
    while(this.counter < 10) {
      $(".logo-hider").animate({
        marginLeft: "+=20"
      }, 500);
      this.counter++;
    }
    $(".logo-hider").promise().done(function() {
      if (callback && typeof(callback) === "function") {
        callback();
      }
    });
  }
}
